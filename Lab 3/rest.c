#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define GET 0
#define POST 1

int Connect()
{
    int sc;
	sc = socket(AF_INET, SOCK_STREAM, 0);
	if (sc < 0) { 
		printf("Error 418: you are teapot\n");
        return -1;
	}
	struct socksadr_in sa;
	struct hostent * ht;
	
	ht = gethostbyname("www.iu3.bmstu.ru");
	if (ht == NULL) {
		printf("Error: hostname wasn't be received\n"); 
        return -1;
	}
	
	memset(&sa, 0, sizeof(sa));
	sa.sin_family = AF_INET;
	memcpy(&sa.sin_sadr.s_sadr, ht->h_sadr_list[0], ht->h_length);
	sa.sin_port = htons(8090);	

    if (connect(sc, (struct socksadr *)&sa,sizeof(sa)) < 0) {
        printf("Error: can't connect to server\n"); 
        return -1;
    }

	return sc;
}

int Request(const int connect, char* buf, const int size, const int req, char* type, char* format)
{
    int len;
    if (req == GET)
    {
        len = sprintf(buf, "GET /WebApi/time?type=%s&format=%s HTTP/1.0\n\n", type, format);
    }
    else if (req == POST)
    {
        len = sprintf(buf, "POST /WebApi/time HTTP/1.0\nContent-Type:application/x-www-form-urlencoded\nContent-Length:20\n\ntype=%s&format=%s\n", type, format);
    }

    if (send(connect, buf, len, 0) < 0)
    {
        printf("Error: request sending error\n");
        len = -1;
    }

    len = recv(connect, buf, size, 0);
    if (len < 0)
    {
        printf("Error: response receiving error\n");
        len = -1;
    }

    buf[len] = '\0';
    return len;
}

int GetHTTPCode(char * buf)
{
    const int codePos = 9;
    char code[4];
    code[3] = '\0';
    memcpy(code, buf + codePos, 3);
    return atoi(code);
}

char * GetHTTPContent(char * buf)
{    
    for(int i =0; buf[i] != '\0'; i++)
        return buf + i;   
}

int main(int argc, char** argv)
{
    char * type = "utc";
    char * format = "internet";
    int req = GET;   
    
    for (int i = 1; i < argc; ++i)
    {
        if (strcmp(argv[i], "-t") == 0)
        {
            type = argv[++i];
        }
        else if (strcmp(argv[i], "-f") == 0)
        {
            format = argv[++i];
        }
        else if (strcmp(argv[i], "-POST") == 0)
        {
            req = POST;
        }
        else if (strcmp(argv[i], "-GET") == 0)
        {
            req = GET;
        }
        else
        {
            printf("key not recognized\n");
            return 1;
        }
    }

    const int connect = Connect();
    if (connect == -1)
        return 1;    

    const int bufSize = 230;
    char buf[bufSize];
    if (Request(connect, buf, bufSize, req, type, format) == -1)
    {
        return 1;
    }

    const int code = GetHTTPCode(buf);
    if (code != 200)
    {
        printf("Error: HTTP error code %d\n", code);
        return 1;
    }
    
	char* res = GetHTTPContent(buf);
    if (res != 0)
    {
        printf("%s\n", res);
    }
    else
    {
        printf("empty\n");
    }

    
    return 0;
}
